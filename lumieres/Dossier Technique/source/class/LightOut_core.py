class LightsOut:
    def __init__(self, hauteur = 8, longueur = 8):
        self.hauteur = hauteur
        self.longueur = longueur
        self.coo = [(x, y) for x in range(longueur) for y in range(hauteur)]
        self.eteintes = []
        self.allumees = [(x, y) for x in range(longueur) for y in range(hauteur)]
        self.fini = False
        self.count = 0

    
    def voisines(self, case):
        '''renvoie liste de voisine de la case'''
        x, y = case
        voisine = [(x, y) for (x, y) in ((x, y+1), (x+1, y), (x, y-1), (x-1, y)) if (x, y) in self.coo]
        return voisine
      
    def action(self, case):
        '''eteint ou allume une lampe et lampe voisine'''
        if case in self.allumees:#si la case est allumes alors on l'y enleve et on la met dans eteintes
            self.allumees.remove(case)
            self.eteintes.append(case)    
        else:
            self.allumees.append(case)
            self.eteintes.remove(case)
            
        for voisine in self.voisines(case):
            if voisine in self.eteintes:#si voisine dans liste des eteint
                self.eteintes.remove(voisine)#alors on l'enleve
                self.allumees.append(voisine)#et on l'allume
            else:
                self.eteintes.append(voisine)
                self.allumees.remove(voisine)
        
        self.count += 1

        if self.allumees == []:
            self.fini = True
        