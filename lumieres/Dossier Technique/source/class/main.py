from kivy.app import App
from kivy.uix.screenmanager import ScreenManager
from menu_screen import MenuScreen
from game_screen import GameScreen
from pause_screen import PauseScreen
from finish_screen import FinishScreen

class LightsOutApp(App):
    def build(self):
        sm = ScreenManager()
        sm.add_widget(MenuScreen(name='menu'))
        sm.add_widget(GameScreen(name='game'))
        sm.add_widget(PauseScreen(name='pause'))
        sm.add_widget(FinishScreen(name='finish'))
        return sm

if __name__ == "__main__":
    LightsOutApp().run()
