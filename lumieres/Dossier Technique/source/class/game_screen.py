from light_out_solver import light_out
from LightOut_core import LightsOut
from kivy.uix.screenmanager import Screen
from kivy.uix.gridlayout import GridLayout
from kivy.uix.button import Button
from kivy.core.window import Window
from kivy.uix.label import Label
from kivy.uix.boxlayout import BoxLayout

class GameScreen(Screen):
    def start_game(self, size):
        self.game = light_out(cotee=size)
        self.layout = GridLayout(cols=self.game.cotee, rows=self.game.cotee)
        for y in range(self.game.cotee):
            for x in range(self.game.cotee):
                button = Button(background_color=(255, 255, 0, 100))
                button.bind(on_release=self.switch_lights)
                self.layout.add_widget(button)
        self.add_widget(self.layout)
        
        # détection des pressions de touches
        self._keyboard = Window.request_keyboard(self._keyboard_closed, self)
        self._keyboard.bind(on_key_down=self._on_key_down)

    def switch_lights(self, instance):
        # Récupère position lumière dans la grille
        index = self.layout.children.index(instance)
        x = index % self.game.cotee
        y = index // self.game.cotee

        self.perform_action((x, y))

    def perform_action(self, coords):
        x, y = coords
        # maj état de la lumière et de c voisines
        self.game.action((x, y))

        # maj interface utilisateur
        for i, button in enumerate(self.layout.children):
            x = i % self.game.cotee
            y = i // self.game.cotee
            if self.game.case[y][x] == 1:  # Si la case est allumée
                button.background_color = (255, 255, 0, 255)  # Jaune
            else:
                button.background_color = (1, 1, 1, 1) 

        if all(val == 0 for row in self.game.case for val in row):  # Si toutes les cases sont éteintes
            self.manager.current = 'finish'

    def _keyboard_closed(self):
        self._keyboard.unbind(on_key_down=self._on_key_down)
        self._keyboard = None

    def _on_key_down(self, keyboard, keycode, text, modifiers):
        if keycode[1] == 's':
            pause_screen = self.manager.get_screen('pause')
            pause_screen.show_solution(None)
        else:
            self.manager.current = 'pause'
