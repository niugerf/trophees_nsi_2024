from kivy.uix.label import Label
from kivy.uix.screenmanager import Screen
from kivy.uix.gridlayout import GridLayout
from kivy.uix.button import Button
from kivy.uix.image import Image
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.floatlayout import FloatLayout
from kivy.lang.builder import Builder
from kivy.app import App


class MenuScreen(Screen):
    def __init__(self, **kwargs):
        super(MenuScreen, self).__init__(**kwargs)
        layout = GridLayout(cols=2)

        #Ajoute l'image en arrière-plan
        self.background = Image(source='../assets/images/Bg_jeu2.jpg', allow_stretch=True, keep_ratio=False)
        self.add_widget(self.background)

        choise = Label(text='Choisissez une taille de plateau', font_size='20', size_hint=(None, None), size=(400, 69))
        layout.add_widget(choise)

        for i in range(2, 9):
            button = Button(text=f'{i}x{i}', font_size="32", size_hint=(None, None), background_color=(1, 1, 1, .7), size=(400, 69))
            button.bind(on_release=self.start_game)
            layout.add_widget(button)
            #center = BoxLayout(pos_hint= {"center_x": 0.5, "center_y": 0.5})
            #self.add_widget(center)
        self.add_widget(layout)


    def start_game(self, instance):
        size = int(instance.text[0])
        self.manager.current = 'game'
        self.manager.get_screen('game').start_game(size)


