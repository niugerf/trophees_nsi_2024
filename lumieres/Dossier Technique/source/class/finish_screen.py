
from LightOut_core import LightsOut
from game_screen import GameScreen
from kivy.uix.screenmanager import Screen
from kivy.uix.gridlayout import GridLayout
from kivy.uix.button import Button
from kivy.uix.label import Label
from kivy.uix.image import Image 

class FinishScreen(Screen):
    def __init__(self, **kwargs):
        super(FinishScreen, self).__init__(**kwargs)
        layout = GridLayout(cols=1)

        # image en arrière-plan
        self.background = Image(source='../assets/images/Bg_jeu.jpg', allow_stretch=True, keep_ratio=False)
        self.add_widget(self.background)

        
        restart_button = Button(text='Restart', background_color=(1, 1, 1, .7), font_size="32", size_hint=(None, None), size=(800, 100))
        restart_button.bind(on_release=self.quit_game)
        layout.add_widget(restart_button)


        self.add_widget(layout)


    def quit_game(self, instance):
        self.manager.current = 'menu'