# TODO: well, that's pretty ugly, we should have used a rotation and inversion matrix
PIECES = [{"0": (0, 5, 5, 5, 5), "90" : (0, -1, -1, -1, -1), "180": (0, -5, -5, -5, -5), "270": (0, 1, 1, 1, 1),
        "b0": (0, 5, 5, 5, 5), "b90" : (0, -1, -1, -1, -1), "b180": (0, -5, -5, -5, -5), "b270": (0, 1, 1, 1, 1)},
        {"0": (0, 5, 5, 5, 1), "90" : (0, -1, -1, -1, 5), "180": (0, -5, -5, -5, -1), "270": (0, 1, 1, 1, -5),
        "b0": (0, 5, 5, 5, -1), "b90" : (0, -1, -1, -1, -5), "b180": (0, -5, -5, -5, 1), "b270": (0, 1, 1, 1, 5)},
        {"0": (0, 1, 1, 1, -1, 5), "90" : (0, 5, 5, 5, -5, -1), "180": (0, -1, -1, -1, 1, -5), "270": (0, -5, -5, -5, 5, 1),
        "b0": (0, -1, -1, -1, 1, 5), "b90" : (0, -5, -5, -5, 5, -1), "b180": (0, 1, 1, 1, -1, -5), "b270": (0, 5, 5, 5, -5, 1)},
        {"0": (0, 1, 5, 1, 1), "90" : (0, 5, -1, 5, 5), "180": (0, -1, -5, -1, -1), "270": (0, -5, 1, -5, -5),
        "b0": (0, -1, 5, -1, -1), "b90" : (0, -5, -1, -5, -5), "b180": (0, 1, -5, 1, 1), "b270": (0, 5, 1, 5, 5)},
        {"0": (0, -1, -1, 5, 5), "90" : (0, -5, -5, -1, -1), "180": (0, 1, 1, -5, -5), "270": (0, 5, 5, 1, 1),
        "b0": (0, 1, 1, 5, 5), "b90" : (0, 5, 5, -1, -1), "b180": (0, -1, -1, -5, -5), "b270": (0, -5, -5, 1, 1)},
        {"0": (0, 5, 5, 1, -5), "90" : (0, -1, -1, 5, 1), "180": (0, -5, -5, -1, 5), "270": (0, 1, 1, -5, -1),
        "b0": (0, 5, 5, -1, -5), "b90" : (0, -1, -1, -5, 1), "b180": (0, -5, -5, 1, 5), "b270": (0, 1, 1, 5, -1)},
        {"0": (0, -1, 5, 5, 1), "90" : (0, -5, -1, -1, 5), "180": (0, 1, -5, -5, -1), "270": (0, 5, 1, 1, -5),
        "b0": (0, 1, 5, 5, -1), "b90" : (0, 5, -1, -1, -5), "b180": (0, -1, -5, -5, 1), "b270": (0, -5, 1, 1, 5)},
        {"0": (0, 5, -1, -1, 5), "90" : (0, -1, -5, -5, -1), "180": (0, -5, 1, 1, -5), "270": (0, 1, 5, 5, 1),
        "b0": (0, 5, 1, 1, 5), "b90" : (0, -1, 5, 5, -1), "b180": (0, -5, -1, -1, -5), "b270": (0, 1, -5, -5, 1)},
        {"0": (0, -1, -5, 5, -1, 5), "90" : (0, -5, 1, -1, -5, -1), "180": (0, 1, 5, -5, 1, -5), "270": (0, 5, -1, 1, 5, 1),
        "b0": (0, 1, -5, 5, 1, 5), "b90" : (0, 5, 1, -1, 5, -1), "b180": (0, -1, 5, -5, -1, -5), "b270": (0, -5, -1, 1, -5, 1)},
        {"0": (0, 5, 5, -5, 1, 1), "90" : (0, -1, -1, 1, 5, 5), "180": (0, -5, -5, 5, -1, -1), "270": (0, 1, 1, -1, -5, -5),
        "b0": (0, 5, 5, -5, -1, -1), "b90" : (0, -1, -1, 1, -5, -5), "b180": (0, -5, -5, 5, 1, 1), "b270": (0, 1, 1, -1, 5, 5)},
        {"0": (0, -1,  5, -1, 5), "90" : (0, -5, -1, -5, -1), "180": (0, 1, -5, 1, -5), "270": (0, 5, 1, 5, 1),
        "b0": (0, 1,  5, 1, 5), "b90" : (0, 5, -1, 5, -1), "b180": (0, -1, -5, -1, -5), "b270": (0, -5, 1, -5, 1)},
        {"0": (0, 5, 5, -5, -1, 1, 1), "90" : (0, -1, -1, 1, 5, -5, -5), "180": (0, -5, -5, 5, -1, 1, 1), "270": (0, 1, 1, -1, 5, -5, -5),
        "b0": (0, 5, 5, -5, -1, 1, 1), "b90" : (0, -1, -1, 1, 5, -5, -5), "b180": (0, -5, -5, 5, -1, 1, 1), "b270": (0, 1, 1, -1, 5, -5, -5)}]

ORIENTATIONS = ("0", "90", "180", "270", "b0", "b90", "b180", "b270")



# HACK: this way is not the best, but hardcoding it is way too long
# TODO: put the pieces and their possible first tile in a file and just load it
def configure_first_possible_tile():

    """For each orientation of each piece, add all possible first tiles where it can be placed, assuming a 12x5 grid."""

    for piece in PIECES:

        for key, value in piece.items():

            possible_tiles = []
            for tile in range(60):

                possible = True
                new_tile = tile
                for move in value[1:]:

                    line = new_tile//5
                    new_tile += move

                    if not 0 <= new_tile < 60:
                        possible = False
                        break

                    if move == 1 or move == -1:
                        if line != new_tile//5: # in this case the line is not the same
                            possible = False
                            break

                if possible:
                    possible_tiles.append(tile)

            piece[key] = (value, tuple(possible_tiles))

# it needs to be done every time the module is imported
configure_first_possible_tile()



def load_piece_set(pieces_set):

        with open("assets/pieces_sets.csv", "r") as f:

            sets = f.readlines()
            choosen_set = sets[pieces_set]

        return [int(x) for x in choosen_set.split(",")]



class Jeu:
    def __init__(self, pieces_set,  height = 3, current_grid = None, occupied = set()):

        if not 3 <= height <= 12:
            raise ValueError("Size given is not a valid grid size")

        self.height = height
        self.remaining_pieces = pieces_set
        self.piece_placer = []
        self.occupied = occupied
        # curr_grid is only used by the AI to place a piece on the current position.
        # TODO: procedural approach may be more efficient, since AI needs to create a lot of Game instances in the current implementation.
        self.grid = current_grid if current_grid else self.create_grid(self.height)



    def __str__(self):

        string = ""
        for case in range(self.height*5):
            string += str(int(self.grid[case])) + " "
            if (case+1)%5 == 0:
                string += "\n"
        return string



    def create_grid(self, height):
        return {cell : False for cell in range(height*5)}



    def placer(self, id, orientation, case):
        if id in self.remaining_pieces:
            newly_used = []
            for vecteur in PIECES[id][orientation][0]:
                case += vecteur
                if (not 0 <= case <= self.height*5-1) or (abs(vecteur) == 1 and newly_used[-1]//5 != (case//5)) or self.grid[case]:
                    return False
                else:
                    newly_used.append(case)

            for case in newly_used:
                self.grid[case] = True

            self.remaining_pieces.remove(id)
            self.piece_placer.append(id)
            return newly_used



    def retirer(self, id, orientation, case):
        if id in self.piece_placer:
            newly_retired = []
            for vecteur in PIECES[id][orientation][0]:
                case += vecteur
                if (not 0 <= case <= self.height*5-1) or (abs(vecteur) == 1 and newly_retired[-1]//5 != (case//5)) or not self.grid[case]:
                    return False
                else:
                    newly_retired.append(case)
            
            for case in newly_retired:
                self.grid[case] = False

            self.remaining_pieces.append(id)
            self.piece_placer.remove(id)
            return newly_retired



    def is_won(self):
        for value in self.grid.values():
            if not value:
                return False
        return True