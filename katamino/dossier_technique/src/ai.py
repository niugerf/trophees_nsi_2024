from core import *



class FoundPathException(Exception):
    pass



class AI:
    def __init__(self, hauteur):

        self.hauteur = hauteur



    def find_winning_path(self, curr_game, path = []):
        if len(path) == self.hauteur: # then game is won
            self.winning_path = path
            raise FoundPathException()

        for child in self.generate_all_valid_children(curr_game.grid, curr_game.remaining_pieces, occupied = curr_game.occupied):
            self.find_winning_path(child[3], path + [(child[0], child[1], child[2])])



    def generate_all_valid_children(self, position, remaining_pieces, occupied):

        already_known_positions = []

        for piece in remaining_pieces:
            for orientation in ORIENTATIONS:
                for tile in [x for x in PIECES[piece][orientation][1] if x < self.hauteur*5 and x not in occupied]:
                    game_copy = Jeu([x for x in remaining_pieces], self.hauteur, {key : value for key, value in position.items()}, {x for x in occupied})
                    if game_copy.placer(piece, orientation, tile):
                        if game_copy.grid not in already_known_positions:
                            yield (piece, orientation, tile, game_copy)
                            already_known_positions.append(game_copy.grid)
