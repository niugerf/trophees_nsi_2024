Le présent projet constitue une implémentation en python du jeu "Katamino".
L'interface graphique est réalisée avec kivy, et une intelligence artificielle
peut vous aider à résoudre le jeu !

I. Installation

Afin d'installer toutes les dépendances requises, il suffit d'exécuter la
commande "pip install -r requirements.txt" à la racine du dossier.

Android :
Il suffit d'installer l'APK, et le tour est joué !

II. Utilisation

Lancer le fichier "main.py" :
    - ligne de commande : python3 main.py
    - IDE : bouton "start"

Choisir "Play".
Sélectioner la taille de grille souhaitée à l'aide du curseur.
Choisir "Save".

Prendre une pièce et la placer - glisser-déposer
Faire pivoter une pièce - touche "O"

Android :
Vous ne trouvez pas la touche "O" ? Pas de problème, vous trouverez sur la
version Android un bouton "rotate". Il vous suffit de maitenir la pièce, et
d'appuyer sur ce bouton. 

III. License

Code placé sous la license GPLv3+.
Texte sous license CC BY-SA.