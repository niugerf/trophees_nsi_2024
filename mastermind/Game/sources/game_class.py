from random import choice, randint
from copy import deepcopy
import csv

class Mastermind :
    """création du fonctionnement du jeu"""
    def __init__ (self) :
        self.solution = None #solution du mastermind, qui va etre générée aléatoirement apres
        self.proposition =  [None for i in range(4)] #la rpoposition est vide au debut
        self.couleurs = [] #on ne met pas de couleurs au depart car on a pas encore defini leur nombre


    
    def generer_solution(self) :
        '''genere une solution aléatoire'''
        self.solution = [choice(self.couleurs) for i in range(4)] #on choisi des couleurs aléatoires parmi la liste de couleurs possibles
        
    
        
    def index_corrects(self, proposition, solution) :
        '''renvoi les index des couleurs bien placées'''
        corrects = []
        for i in range(4) :
            if proposition[i] == solution[i] : #si une couleur de la propisiton correspond à celle de la solution
                corrects.append(i) #on ajoute l'index de la couleur bien placée à la liste
                
        return corrects
    
    
    def reset_proposition(self) :
        '''reinitialise self.proposition'''
        self.proposition =  [None for i in range(4)] #on remet la position à une liste de None
    
    
    
    def reset_solution(self) :
        '''reinitialise self.solution'''
        self.solution =  [None for i in range(4)]  #on remet la solution à une liste de None
    
    def bien_places(self, proposition, solution) :
        '''renvoi le nombre de couleurs bien placés'''
        return len(self.index_corrects(proposition, solution)) #on renvoi le nombre d'index de couleurs bien placés
    
    
    
    def mal_places(self, proposition, solution) : 
        '''renvoi le nombre de couleurs mal placées'''
        proposition, solution = proposition[:],solution[:] #on copie la proposition de la solution
        
        index_solution = self.index_corrects(proposition, solution) #on recupere les index des couleurs bien places
        

            
        index_solution.reverse()
        for index in index_solution : #on supprime toutes les couleurs bien places de salution et proposition pour ne garder que celles mal placés
            solution.pop(index)
            proposition.pop(index)
        
        mal_places =0
        for i in range(len(solution)) : 
            for k in range(len(solution)) :
                if proposition[i] == solution[k] : #si une couleur de la proposition correspond à celle de la solutoin
                    solution[k] = None #on enleve la couleur de la solution pour ne pas qu'elle soit compté 2 fois
                    mal_places += 1
                    break #on arrete la boucle pour ne pas compter 2 fois une meme couleur
                                        
        return mal_places
    


    def resoudre(self) :
        '''resoud le mastermind : renvoi la liste de toutes les propositions de l'ordinateur '''
        lst_possibles = [[couleur1, couleur2, couleur3, couleur4] for couleur1 in self.couleurs for couleur2 in self.couleurs for couleur3 in self.couleurs for couleur4 in self.couleurs] #on créé la liste de toutes les solutions possibles
        propositions = []
        while len(lst_possibles) > 1 and self.proposition != self.solution : #tant qu'on a pas trouvé la solution
            self.proposition = choice(lst_possibles) #on choisi une proposition de couleurs au hasard dans celles possibles
            propositions.append(self.proposition) #on l'ajoute à la liste de propositions
            mal_places = self.mal_places(self.proposition, self.proposition) #on recupere le nombre de couleurs mal placés et bien placés
            bien_places = self.bien_places(self.proposition, self.solution)
            
            lst_possibles = [possibilite for possibilite in lst_possibles if self.mal_places(possibilite, self.proposition) == self.mal_places(self.proposition, self.solution) and self.bien_places(possibilite, self.proposition) == self.bien_places(self.proposition, self.solution) ] #on retire les propositions inpossibles dans la liste de proposition : explication + détaillée dans la documentation
            
        

        propositions.append(lst_possibles[0]) #on ajoute le seul élément restant de la liste de proposition possibles, c'est donc la solution
        
        if len(propositions) > 1 :
            return propositions if propositions[-1] != propositions[-2] else propositions[:-1] #on retire le dernier element si les 2 derniers sont les memes

class Jeu : #classe pour stocker différentes informations relatives à la partie
    def __init__(self) :
        self.nom = "player" + str(randint(1000, 9999)) #nom du joueur
        self.page_ouverte = True #si la page du jeu est ouverte
        self.jeu_en_cours = False #True si le jeu est en cours, donc si le joueur n'est pas dans l'accueil
        
        self.couleurs = [] #on ne met pas de couleurs au depart car on a pas encore defini leur nombre
        self.nb_couleurs = 6 #6 couleurs par défaut dans le jeu
        self.jeu_contre_ordi = False #si le joueur a activé le jeu contre l'ordinateur
        self.tour_ordi = False #si l'ordinatuer est le decodeur
        
        self.ligne_actuelle = 0 #ligne à laquelle est rendu le joueur
        self.score = 0 #score du joueur
        self.nb_parties = 0 #nombre de parties faites par le joueur
        
        self.tps_jeu = 0 #variable qui represente le temps de chaque partie pour le chronometre
        self.chrono_en_cours = False #si le charono est lancé
        
 
        self.table_score = list(csv.reader(open("assets\score.csv"))) #on récupere le tableau des scores

            
    
    def ajout_score(self, victoire = True, ligne_actuelle = 0) :
        '''ajout de points au score en cas de victoire'''
        if victoire is True : #si victoire du joueur 
            self.score += 11 - ligne_actuelle #on ajoute des points en fonction du nombre de coups qu'a mis le joueur pour trouver la solu
            
    def set_couleurs(self, nb_couleurs) :
        '''créé la liste de couleurs en fonction du nombre choisi'''
        self.couleurs = ["GREEN", "BLUE", "YELLOW", "PURPLE", "WHITE", "RED",  "INDIGO", "ORANGE",  "TEAL","BROWN"][:nb_couleurs]
        self.nb_couleurs = nb_couleurs
    
    
    
    
    