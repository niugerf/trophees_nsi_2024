
import flet as ft
import csv
from time import sleep
from random import shuffle
from datetime import datetime
from game_class import*




def main(page : ft.page) : #on definit une classe pour l'inteface graphique

    j = Jeu()
    m = Mastermind()
    
########################################### caracteristiques graphiques ##############################################
    
    
    page.title = "Mastermind"
    page.window_width = 800 #largeur de la page
    page.window_height = 1000 #hauteur de la page
    page.padding = 10  # espacement au bord
    
    page.fonts = {
        "Kanit": "assets/fonts/Kanit_Bold.ttf",
        "Impact" : "assets/fonts/Impact.fft",
    }
    
    
    page.window_resizable = False #on ne peut pas redimentionner la page
    page.bgcolor = "#edede9" #couleur de fond du jeu
    page.window_prevent_close = True #alerte au moment de fermer la page
    page.scroll = True #permet de defiler la page dans le cas ou l'ecfrant du joueur n'est pas assez grand
    

    page.theme_mode = ft.ThemeMode.DARK
    

    

##### creation de differentes couleurs utilisés dans le jeu #####
    
    color1 = "#6096ba"
    color2 = "#bfdbf7"
    color3 = "WHITE"
    color4 = "BLACK"
    color5 = "GREY"
    color6 = "#274c77"
    
    text_color_1 = "#cfdbd5"
    text_color_2 = "BLACK"
    text_color_3 = "RED"

    
    
##################################### parametres de fermeture de page ####################################################    
    
    
    def window_event(e):
        if e.data == "close": #si l'evenement sur la page est le click sur le bouton quitter
            alert_close.open = True #on ouvre l'alerte de fermeture
            page.update()


            
    def quitter_click(e): 
        '''en cas de click sur oui dans l'alerte de fermeture'''
        page.window_destroy() #on quitte la fenetre
        
        if alert_close.content.value and j.jeu_en_cours: #si le joueur veut enregistrer son score
            add_score_csv("assets/score.csv", j.table_score) #
        j.page_ouverte = False #on met l'attribut j.page_ouverte a False pour arreter le chronometre


        
    def annuler_click(e):
        '''en cas de click sur non dans l'alerte de fermeture'''
        alert_close.open = False #on ferme l'alerte en laissant la fenetre ouverte
        page.update()

    
    alert_close = ft.AlertDialog( #creation de l'alerte de fermeture
        # modal=True,
        
        title=ft.Text("Attention ! vous allez quitter le jeu"), #titre de l'alerte
        content=ft.Switch(label="sauvegarder le score", value=False), #contenu de l'alerte
        actions=[ #les 2 boutons dans l'alerte
            ft.TextButton("Annuler", on_click=annuler_click), #on execute la fonction annuler_click si le joeur a cliqué sur non
            ft.TextButton("Quitter", on_click=quitter_click), #on execute la fonction quitter_click si le joeur a cliqué sur oui
        ],
        actions_alignment=ft.MainAxisAlignment.END, #pour aligner les boutons à gauche de l'alerte
    )    
       
    page.add(alert_close) #ajout de l'alerte de fermeture à la page
    
    
    page.on_window_event = window_event #en cas d'evenement sur la fenetre on execute la fontion window_event
    
    
############################################# detection des touches ##########################################

    def on_keyboard(e):
        if e.key == "Escape" : #si le joueur a appuyé su rla touche "echap"
            alert_close.open = True #on ouver l'alerte de fermeture
        
        if e.key == "Enter" and not container_boutons.content.controls[1].disabled : #si le joueur a appuyé su la touche "entrée" et que le bouton valider est activé
            callback_valider(None) #on execute la fonction callback du bouton valider
        
        page.update()
        
        
    page.on_keyboard_event = on_keyboard #en cas d'evenement sur le claver on execute la fonction on_keyboard

    
    

    
######################################### fonctions graphiques #############################################

    
    

    def activer_ligne(ligne, desactive = False) :
        '''active le click et le contour de la premiere ligne'''
        for i in range(4) :
            case = container_jeu.content.controls[18-2*ligne].controls[2+i] #selectionne au fur et à mesur une des 4 cases du numero de ligne entrée en parametres
            case.disabled = desactive #active ou non le click en fonction de desactive entré en parametres
            case.border = ft.border.all(3, color4) #on ajoute une bordure au cases
        
        page.update()
    

    
    def ligne_solution(desactive) :
        '''active ou desactive le click et le contour de la ligne de solution'''
        for i in range(4) :
            case = row_infos.controls[0].content.controls[i] #selectionne au fur et à mesur une des 4 cases de la ligne de solution
            case.disabled = desactive #on active le click de la case
            case.border = ft.border.all(3, color4) #on ajoute une bordure à la case
            
        page.update()
    
    
    def affichage_solution() :
        '''affiche la solution dans la ligne row_infos'''
        controls_solution = [] #on créé une liste qui va contenir les 4 cases de la solution
        for i in range(4) : #on créé au fur et à mesure 4 cases qui representent la solution 
            case = ft.Container(
                width=40, 
                height=40, 
                bgcolor=m.solution[i], #la couleur de la case correspond à une couleur de la soltion
                margin= 6, 
                border_radius = 35,
                on_click = callback_case,
                disabled = True,
                border = ft.border.all(3, color4))
            
        
            case.emplacement = (None,i) #on definit un attribut emplacement à la case 
            case.i_couleur = 0 #indice de couleur de la case pour pouvoir changer la couleur d'arrière plan de la case
        
            controls_solution.append(case) #on ajoute la case que l'on vient de créer à la liste de cases
        
        container_solution = row_infos.controls[0] #on recuperre l'emplacement du container qui affiche la soluton
        
        container_solution.content = ft.Row(controls_solution) #on remplace le texte "SOLUTION" par les 4 cases de controls_solution correspondants à la solution
        container_solution.padding = ft.padding.only(20) 

        
        page.update()

    
 
    def reset_jeu(victoire) :
        '''reinitialise les cases et les variables associés à 1 tour'''
        
        ### reinitialisation des elements graphiques ###
        
        boutons = container_boutons.content.controls #on recupere l'emplacement des boutons du bas de page (valider, recommencer et quitter)
        for h in range(10) :
            cases = container_jeu.content.controls[18-2*h].controls
            for i in range(4) :
                
                cases[2+i].bgcolor = color5 #on remet l'arrière plan de toutes ls cases en gris
                cases[2+i].border = None #on enleve la bordure de toutes les cases
                cases[2+i].i_couleur = 0 #on remet l'index de couleur de chaque case à 0
                cases[2+i].disabled = True #on desactive le clic toutes les cases
                cases[0].bgcolor = None  #on enleve l'arrière plan rouge ou vert derriere le numero de ligne
                cases[7].controls[i].bgcolor = color5 #on remet les cases d'infos de bien/mal placés en gris
                
        txt_places(0,0) #on remet le texte d'affichage des bien et mal placés a 0
        j.ajout_score(victoire, j.ligne_actuelle) #on ajoute des points au joueur avec la classe jeu en fonction de si le joueur à gagné ou non
        j.ligne_actuelle = 0 #on remet la ligne actuelle à 0
        
        container_score.content.controls[0].value = f'''score : {j.score} points''' #on actualise l'affichage du score
       
        container_solution = row_infos.controls[0] #on recuperre l'emplacement du container qui affiche la soluton
        container_solution.content = ft.Text("SOLUTION", size = 30, color = text_color_1, font_family = "Kanit") #on remet le texte "SOlUTION" pour cacher la bonne reponse
        container_solution.padding = ft.padding.only(20)
        
        ### reinitialisation des attributs des classes Mastermind et Jeu ###
        
        m.reset_proposition() #on reinitialise la proposition du joeur (ou de l'ordinateur)
        j.chrono_en_cours = False #on arrete le chrono
        j.tps_jeu = 0 #on remet le chrono à 0
        row_infos.controls[1].content.value = "  temps : 0'00''" #on remet l'affichage du chronometre à 0
        
        boutons[0].disabled = False #on reactive le bouton valider et recommencer (dans le cas ou ils ont été descativés)
        boutons[1].disabled = False  
        
        ### gestion du prochain decodeur (joueur ou ordinateur) ###
        
        if j.jeu_contre_ordi and not j.tour_ordi : #si le jeu contre l'ordinateur est activé et que au tour precedent ce n'etait pas a l'ordinatuer de jouer

            m.reset_solution() #on reinitialise la proposition car ce sera le joueur qui va la choisir apres
            j.tour_ordi = True #l'ordinateur deviens le decodeur
            affichage_solution() #on affiche la solution, qui est donc actuellement vide
            ligne_solution(False) #on active le click de la ligne d esolution pour que le joueur puisse choisir la solution
            
            
        else : #si le prochain tour est celui de joueur
            j.tour_ordi = False #ce n'est dont pas au tour de l'ordinateur
            activer_ligne(0) #on active la premiere ligne pour que le joueur fasse une proposition de couleurs
            m.generer_solution() #on genere une nouvelle solution
            
        if not j.tour_ordi and not j.jeu_contre_ordi or j.tour_ordi and j.jeu_contre_ordi :
            j.nb_parties += 1
        
        print("solution tour ", 1 + j.nb_parties, " : ", m.solution) #affichage de la solution pour faciliter les tests et verifier le bon fonctionnement du jeu
    
        page.update()
        
        
        
    def txt_places(bien_places, mal_places) :
        '''met à jour le texte qui indique les cases bien et mal places'''
        txt_bien_places = column_infos.controls[1].content #on definit l'emplacement du texte à modifier
        txt_mal_places = column_infos.controls[2].content
        if bien_places < 2 :  #on affiche le nombre de couleurs biens et mals placés
            txt_bien_places.value = f'''{bien_places} couleur bien placée'''
        else :
            txt_bien_places.value = f'''{bien_places} couleurs bien placées'''
        if mal_places < 2 :
            txt_mal_places.value = f'''{mal_places} couleur mal placée'''
        else :
            txt_mal_places.value = f'''{mal_places} couleurs mal placées'''
            
            
            
            
    def resolution() :
        boutons = container_boutons.content.controls #on definit la ligne de boutons en bas de page
        lst_propositions = m.resoudre() #on recupère les propositions de l'ordinateur
        boutons[0].disabled = True #on desactive les boutons pour eviter des erreurs
        boutons[1].disabled = True
        ligne_solution(True) #on desactive la possibilitée de modifier la proposition


        for i in range(4) :
            container_jeu.content.controls[18].controls[2+i].border = ft.border.all(3, color4) #on met un contour à la première ligne de cases     

        for proposition in lst_propositions : #on boucle sur toutes les propositions de l'ordinatuer qui vont permettre d'arriver à la solution
            cases = container_jeu.content.controls[18-2*j.ligne_actuelle].controls #on defini la ligne de cases en cours
            cases_suivantes = container_jeu.content.controls[16-2*j.ligne_actuelle].controls #meme chose mais pour les cases suivantes

            sleep(1) 
            m.proposition = proposition #on definit la proposition du mastermind par la proposition de couleurs faite par l'ordinateur

            index = [i for i in range(4)] 
            shuffle(index) #on créé une liste melangée aléatoirement avec les valeurs 1, 2, 3, 4 qui vont servir d'ordre par lequel l'ordinateur remplis les cases    
                            #pour que le jeu semble plus "naturel"
            for i in index :                        
                cases[2+i].bgcolor = m.proposition[i] #l'ordinateur colorie les cases de la ligne actuelle dans un ordre aléatoire
                sleep(1)

            page.update()
            sleep(1)  
            bien_places = m.bien_places(m.proposition, m.solution) #on calcule les couleurs bien places par l'ordinateur
            mal_places = m.mal_places(m.proposition, m.solution) #on calcule les couleurs mal places par l'ordinateur
            txt_places(bien_places, mal_places) #on met a jour le texte en fonction du nombre de cases bien en mal placés

            for i in range(4) :
                cases[2+i].border = None #on enleve les bordures de la ligne en cours
                cases_suivantes[2+i].border = ft.border.all(3, color4) #on active les bourdures de la ligne suivante

                if bien_places > i : #on colorie les petites cases en bout de ligne en fonction du nombre de couleurs bien et mal places
                    cases[7].controls[i].bgcolor = "BLACK" #noir pour les couleurs bien places
                elif bien_places + mal_places > i :
                    cases[7].controls[i].bgcolor = "WHITE" #blanc pour les couleurs mal places

            cases[0].bgcolor = "RED" #on colorie en rouge derriere le numero de ligne
            j.ligne_actuelle += 1 #on passe à la ligne suivante 


            page.update()

        container_jeu.content.controls[20-2*j.ligne_actuelle].controls[0].bgcolor = "GREEN"  #on colorie en vert derriere le dernier numero de ligne car c'est la bonne reponse

        page.update()

        sleep(2)
        alert_victoire_ordi.open = True #on active l'alerte de victoire de l'ordinateur
                    

#################################################### fonctions callback ################################################
    
    
    
    def callback_valider(e) :
        '''fonction callback du bouton valider, qui permet de passer à la ligne suivante dans le jeu'''
               
        ###si l'ordinateur est le decodeur => jeu automatique ###
        
        if j.tour_ordi : #si le décodeur est l'ordinateur
            if None in m.solution : #si la solution proposée par le joueur n'est pas complète
                alert_incomplet.open = True #on affiche l'alerte en cas de proposition incomplete
            else :
                resolution()
            
            
        ### si le joueur est le decodeur ###
        
        else :
        
            if None in m.proposition : #si la proposition du joueur n'est pas complète
                alert_incomplet.open = True  #on affiche l'alerte en cas de proposition incomplete
            else :
                boutons = container_boutons.content.controls #on definit la ligne de boutons en bas de page
                bien_places = m.bien_places(m.proposition, m.solution) #on calcule les couleurs bien places par le joueur
                mal_places = m.mal_places(m.proposition, m.solution) #on calcule les couleurs mal places par le joueur
                txt_places(bien_places, mal_places) #on met a jour le texte en fonction du nombre de cases bien en mal placés
                
                cases = container_jeu.content.controls[18-2*j.ligne_actuelle].controls #on defini la ligne de cases en cours
                cases_suivantes = container_jeu.content.controls[16-2*j.ligne_actuelle].controls  #meme chose mais pour la ligne de cases suivantes
                
                
                for i in range(4) : #on boucle 4 fois pour les 4 cases de chaque ligne

                    if bien_places > i : #on colorie les petites cases en bout de ligne en fonction du nombre de couleurs bien et mal places
                        cases[7].controls[i].bgcolor = "BLACK" #noir pour les couleurs bien places
                    elif bien_places + mal_places > i : 
                        cases[7].controls[i].bgcolor = "WHITE" #blanc pour les couleurs mal places
                        
                    
                    cases[2+i].disabled = True #on desactive le click de la ligne en cours
                    cases[2+i].border = None #on enleve les bordures de la ligne en cours
                    cases[0].bgcolor = "RED" #on colorie en rouge derriere le numero de ligne
                    
                    cases_suivantes[2+i].disabled = False #on active le click des cases de la ligne suivante
                    cases_suivantes[2+i].border = ft.border.all(3, color4) #on met une bordure aux cases de la ligne suivante

                m.reset_proposition() #on reinitialise la proposition du joueur pour qu'il puisse en faire une nouvelle sur la ligne de cases suivantes
                j.ligne_actuelle += 1

                
                ## verfication de la victoire ou de la defaite du joueur ##
                
                if bien_places == 4 : #si le joueur gagne
                    cases[0].bgcolor = "GREEN" #on colorie en vert derriere le numero de ligne car le joueur a trouve la bonne reponse
                    j.chrono_en_cours = False #on arrete le chrono
                    activer_ligne(j.ligne_actuelle, True) #on enleve le click de la ligne suivante pour eviter des erreurs
                    affichage_solution() #on affiche la salution dans le "container_solution"
                    
                    boutons[0].disabled = True #on desactive les boutons du bas de page pour eviter des erreurs 
                    boutons[1].disabled = True
                    
                    sleep(2)
                    alert_victoire.content.value = f'''Victoire en {j.tps_jeu//60} minutes et {j.tps_jeu%60} secondes, \nvous remportez {11 - j.ligne_actuelle} points''' #on met à jour le texte de l'alerte de victoire du joueur
                    alert_victoire.open = True #on affiche l'alerte de victoire du joueur

                if j.ligne_actuelle == 10 : #si le joueur perds
                    j.chrono_en_cours = False #on arrete le chrono
                    activer_ligne(j.ligne_actuelle, True) #on enleve le click de la ligne suivante pour eviter des erreurs
                    affichage_solution() #on affiche la salution dans le "container_solution"

                    boutons[0].disabled = True #on desactive les boutons du bas de page pour eviter des erreurs 
                    boutons[1].disabled = True
                    
                    sleep(2)
                    alert_defaite.content.value = f'''Défaite en {j.tps_jeu//60} minutes et {j.tps_jeu%60} secondes, \nvous ne remportez point''' #on met à jour le texte de l'alerte de victoire du joueur
                    alert_defaite.open = True #on affiche l'alerte de defaite du joueur
                    
        page.update()
            
       

    def callback_close(e) :
        '''fonction callback du bouton quitter'''
        alert_close.open = True #on ouvre l'alerte de fermeture
        page.update()
    

    
    def callback_reset(e) :
        '''fonction callback du bouton recommencer'''
        if j.tour_ordi :
            for i in range(4) :
                case = row_infos.controls[0].content.controls[i] #selectionne au fur et à mesur une des 4 cases du numero de ligne entrée en parametres
                case.bgcolor = None #active ou non le click en fonction de desactive entré en parametres
                case.i_couleur = 0 #on ajoute une bordure au cases
                m.solution = [None for i in range(4)]
            
            
        else : 
            for i in range(4) :
                case = container_jeu.content.controls[18-2*j.ligne_actuelle].controls[2+i] #selectionne au fur et à mesur une des 4 cases du numero de ligne entrée en parametres
                case.bgcolor = color5 #active ou non le click en fonction de desactive entré en parametres
                case.i_couleur = 0 #on ajoute une bordure au cases
                m.proposition = [None for i in range(4)]
        
        page.update()
    
    
   
    def callback_case(e) :
        '''fonction callback des cases avec choix de couleur, change la case de couleur au clic'''
        container = e.control
        
        if j.ligne_actuelle == 0 and m.proposition == [None for i in range(4)] : #si c'est le premier clic de la ligne
            j.chrono_en_cours = True  #on lance le chrono
        
        container.bgcolor =  j.couleurs[container.i_couleur%j.nb_couleurs] #on met à jour la couleur de la case
        if j.tour_ordi : #si le joueur definit la solution que doit trouver l'ordinateur
            m.solution[container.emplacement[1]] = j.couleurs[container.i_couleur%j.nb_couleurs] #on met à jour la solution
            
        else :
            m.proposition[container.emplacement[1]] = j.couleurs[container.i_couleur%j.nb_couleurs] #sinon c'est au joueur de jouer donc on met à jour la proposition
        
        container.i_couleur += 1 #l'indice de couleur du container augmente de 1
        
        page.update()
        
        

    
    def affichage2_solution(e) :
        '''fonction de test'''
        if not j.tour_ordi :
            affichage_solution()
    
    
    
    

############################################# fonctions callback de la page d'accueil ############################



    def callback_mode_jeu(e) :
        if j.jeu_contre_ordi is False :
            j.jeu_contre_ordi = True #on inverse le mode de jeu
            column_accueil.controls[1].controls[0].border = None #on enleve la bordure de l'autre bouton
            column_accueil.controls[1].controls[1].border = ft.border.all(3, "BLACK") #on met une bordure au mode de jeu selectionné
        
        
        else :
            j.jeu_contre_ordi = False #on inverse le mode de jeu
            column_accueil.controls[1].controls[0].border = ft.border.all(3,"BLACK") #on met une bordure au mode de jeu selectionné
            column_accueil.controls[1].controls[1].border = None #on enleve la bordure de l'autre bouton
            
        page.update()
    
    
    
    def callback_couleur_moins(e) :
        '''fonction callback du boutons couleur - '''
        if j.nb_couleurs > 2 : #on diminue le nombre de couleur si il est superieur à 2
            j.nb_couleurs -= 1
            column_accueil.controls[2].controls[1].content.value = str(j.nb_couleurs) + " couleurs" #on affiche le nombre de couleurs choisis
        page.update()
            
            
    def callback_couleur_plus(e) :
        '''fonction callback du boutons couleur + '''
        if j.nb_couleurs < 10 : #on augmente le nombre de couleur si il est inferieur à 10
            j.nb_couleurs += 1
            column_accueil.controls[2].controls[1].content.value = str(j.nb_couleurs) + " couleurs" #on affiche le nombre de couleurs choisis
        page.update()
            
            
            
    def callback_lancer_jeu(e) :
        '''fonction callback du bouton lancer jeu'''
        j.jeu_en_cours = True #on passe l'attribue jeu en cours de la classe j à True pour permettre de lancer le jeu
        page.controls.pop(1) #on supprime la page d'accueil de la page
        j.tps_jeu = 0 #on remet le chronometre à 0
        page.update()
         
            
            
    def callback_tableau_score(e) :
        '''fonction callback du bouton tableau score'''
        alert_tableau_score.open = True
        page.update()
        
    
    
    def controler_pseudo(e) :
        '''verfication de la validité du pseudo proposé par le joueur'''
        field = e.control
        pseudo = field.value
        if len(pseudo) > 10 : #si le pseudo fait plus de 10 caracteres
            pseudo = pseudo[:10] #on le réduit à 10 caractères maximum 
            field.value = pseudo #on met a jour le pseudo dans le champ de texte
        
        j.nom = pseudo
        page.update()
    

############################### elements graphiques de la page d'accueil #########################################
        

    column_accueil = ft.Column([ #colonne qui va contenir tous les elements de la page d'accueil
        ft.Container(ft.Text("MASTERMIND", size = 110, color = color4, font_family="Impact"), margin = ft.margin.only(0, -20, 0, -10)), #texte "MASTERMIND" en haut de la page d'accueil
        
        
        ft.Row([ #ligne qui contient les boutons de selection du mode solo ou 1vs1
            ft.Container( #container qui contient le texte "mode solo"
                content = ft.Text("mode solo", size = 20, color = text_color_2),
                height = 60, 
                width = 130, 
                bgcolor=color1,  
                border = ft.border.all(3,"BLACK"),
                border_radius = 10,
#                 margin = ft.margin.only(0,-1000,0,0),
                on_click = callback_mode_jeu, #on execute la fonction pour changer de mode de jeu au click sur le container
                alignment = ft.alignment.center),
            
            ft.Container(  #container qui contient le texte "mode 1vs1"
                content = ft.Text("mode 1vs1", size = 20, color = text_color_2),
                height = 60, 
                width = 130, 
                bgcolor=color1,
                border = None,
                border_radius = 10,
                on_click = callback_mode_jeu, #on execute la fonction pour changer de mode de jeu au click sur le container
                alignment = ft.alignment.center)],
            
                alignment = ft.MainAxisAlignment.CENTER),
        
        
        ft.Row([ #ligne qui contient les boutons de selection du nombre de couleurs et l'affichage du nombre de couleurs selectionnées
            ft.Container( #container du bouton couleurs moins
                height = 100, 
                width = 100, 
                on_click = callback_couleur_moins, #on execute la fonction pour reduire le nombre de couleurs au click
                content = ft.Text("-", size = 70, color = text_color_2),
                bgcolor=color1,  
                border_radius = 10,
                alignment = ft.alignment.center),
            
            ft.Container(#container qui affiche le nombre de couleurs
                content = ft.Text(str(j.nb_couleurs) + " couleurs", size = 30, color = text_color_2, text_align = ft.TextAlign.CENTER), #on afiche le nombre de couleurs selectionnées
                height = 100, 
                width = 120,
                bgcolor=color1,
                border_radius = 10,
                margin = 10,
                alignment = ft.alignment.center),
            
            ft.Container( #container du bouton couleurs plus
                content = ft.Text("+", size = 70, color = text_color_2),
                height = 100,
                width = 100, 
                bgcolor=color1,
                border_radius = 10,
                on_click = callback_couleur_plus, #on execute la fonction pour augmenter le nombre de couleurs au click
                alignment = ft.alignment.center)],
            
            alignment = ft.MainAxisAlignment.CENTER),
        
        
        ft.Container(#container qui sert de bouton pour lancer le jeu
            content = ft.Text("lancer le jeu", size = 40, color = text_color_1),
            height = 100, 
            width = 250,
            bgcolor = color6,
            border_radius = 10,
            margin = ft.margin.only(0,10,0,20),
            on_click = callback_lancer_jeu, #on execute la fonction pour lancer le jeu au click
            alignment = ft.alignment.center),
        
        ft.TextField(width = 270, label = "Pseudo", on_change = controler_pseudo, text_size = 30, text_align = "CENTER", color = "#0061A4", disabled = False),
        
        
        ft.Container(ft.Text( #container qui contient les regles du jeu
            f'''REGLES DU JEU :\n
Le jeu se joue à deux : un codeur (ici l'ordinateur) et un décodeur.\n
Le Mastermind est un jeu où un joueur crée un code secret de couleurs et l'autre joueur essaie de le deviner. Le codeur choisit une séquence de 4 couleurs (différentes ou non), et la cache au décodeur.\n
Ce dernier propose des combinaisons de couleurs, et le codeur fournit des indications sur la justesse des propositions. Les pions noirs indiquent les couleurs correctes et bien placées, tandis que les pions blancs indiquent les couleurs correctes mais mal placées. \n
Le décodeur utilise ces indices pour faire de nouvelles propositions et tenter de découvrir le code secret en 10 tentatives maximum. \n
Le jeu continue jusqu'à ce que le décodeur découvre la combinaison secrète ou atteigne un nombre maximum de tentatives.''',
            weight=ft.FontWeight.BOLD,
            color = text_color_2,
            text_align = ft.TextAlign.CENTER, ), 
                     bgcolor = color2, 
                     padding = 12,                      
                     border_radius = 10, 
                     margin = ft.margin.all(10),
                     alignment = ft.alignment.center)
    ],
        
        horizontal_alignment = ft.CrossAxisAlignment.CENTER,)
            
        
    page.add(column_accueil) #ajout de l'element à la page
    
    
    page.update()
    
    while not j.jeu_en_cours and j.page_ouverte :
        pass #on attends que le joueur ai choisi son mode de jeu et le nombre de couleurs
    
    
    page.title = "Mastermind" + 10*" " + "joueur : " + j.nom 
    j.set_couleurs(j.nb_couleurs) #on definit les couleurs qui vont etre utilisés en foction de combien le joueur en a choisi
    m.couleurs = j.couleurs
    m.generer_solution() #on genere une solution en fonction des couleurs qui ont été choisi
    
    
    
    print("solution tour ", 1 + j.nb_parties, " : ", m.solution)  #affichage de la solution pour faciliter les tests et verifier le bon fonctionnement du jeu

    
############################################## elements graphiques ###########################################    



##### création du container_score (première "ligne" en haut de la page)    
    
    content_container_score = [ft.Text( #liste qui va serir pour le contenu du "container score"
                                    "score : 0 points", #affichage du score en haut à gauche de la page
                                    size=32,
                                    color = text_color_1, font_family = "Kanit"),
                               
                               ft.Container(
                                    width = 10,
                                    height = 40, 
                                    bgcolor = color4,
                                    border_radius = 10, 
                                    margin = ft.margin.only((10 - j.nb_couleurs)*45 )) #on definit une marge qui varie en fonction du nombre de couleurs
                              ]
    
    for i in range(j.nb_couleurs) :
        content_container_score.append(ft.Container( #on ajoute une case pour chaque couleur
            width=35, 
            height=35, 
            bgcolor=j.couleurs[i], #chaque case à une couleur possible en arriere plan
            border = ft.border.all(3, color4),
            border_radius = 35))
    
    
    container_score = ft.Container(#on créé un container qui va contenir le "content container score" dans une ligne
        content= ft.Row(content_container_score), #on créé une ligne qui a pour contenu la liste créé plus tot
        width=740,
        bgcolor=color1, #on met un arrière plan jaune au container
        padding = ft.padding.only(20), 
        border_radius = 10)
    
    
    page.add(container_score) #ajout de l'element à la page 

    
    
##### création du "row_infos" (deuxième ligne en haut de la page)

    
    row_infos = ft.Row([ #création de la deuxième ligne en parant du haut de la page
        ft.Container( #ajout d'un container qui represente la solution (cachée)
            content = ft.Text("SOLUTION", size = 30, color = text_color_1, font_family = "Kanit"), #texte du container : "SOLUTION"
            width = 280, 
            height = 55,
            bgcolor = color1,
            border_radius = 10,
            margin = ft.margin.only(80),
            alignment=ft.alignment.center),
        
        ft.Container( #ajout d'un containe rqui represente le chronometre
            content=ft.Text("  temps : 0'00''",  size = 22, color = text_color_1, font_family = "Kanit"), #texte du chronometre, initiallement mis à 0'00''
            width = 150, 
            height = 35, 
            bgcolor= color1,       
            border_radius = 10, 
            margin = ft.margin.only(200)),
    ])
    
    
    page.add(row_infos) #ajout de l'element à la page
    

    
##### création du container_jeu ("colonne" gauche de la page qui contient les cases du jeu, les numeros de cases...)
    
        
    container_separateur = ft.Container( #on créé un container de 1px de haut pour séparer les lignes de cases entres elles
        width = 330, 
        height = 1, 
        bgcolor = color4,
        margin = ft.margin.only(20))
    
    
    container_separateur_2 = ft.Container( #séparateur à gauche des numeros
        width = 3,
        height=35,
        bgcolor = color4)
    
    

    
     #on créé une liste qui va contenir toutes les lignes de cases
    controls_container_jeu = []
    for h in range(10) : #on boucle 10x pour les 10 lignes de cases  
        controls_cases = [ #on cree le numero de colonne et le separateur à gauche du numero
            ft.Container(
                content =  ft.Text(
                    str(10-h), #numero de la ligne
                    height = 35,
                    size=30, 
                    color = color4,
                    font_family = "Kanit"), 
                width = 35, 
                padding = ft.padding.only(0,0,0,10),
                border_radius = 10,
                margin = ft.margin.only(20),
                alignment = ft.alignment.center),
            container_separateur_2 #on ajoute un separateur vertical à droite des numeros
        ]

        
        for i in range(4) : #on boucle 4x pour les 4 cases dans chaque ligne
            case_cliquable = ft.Container( #on créé une case cliquable
                width=35, 
                height=35, 
                bgcolor=color5,
                margin= 6, 
                border_radius = 35, 
                on_click=callback_case , #on execute la fonction de click de case au click
                disabled = True)
            
            case_cliquable.emplacement = (9-h, i) #on définit les coordonnées de la case
            case_cliquable.i_couleur = 0 #on définit l'index de couleur de la case
            controls_cases.append(case_cliquable) #on ajoute a chaque fois une case à la ligne de cases
        
        controls_cases.append(container_separateur_2) #on ajoute un separateur vertical à droite des cases 
        
        controls_cases.append(ft.Row([ft.Container(#on ajoute une ligne composé de 4 petites cases pour afficher les couleurs bien/mal placés
            height = 10, 
            width = 10, 
            bgcolor = color5, 
            border_radius = 3,
            margin = 2) for i in range(4)], 
                                     width = 50, 
                                     wrap = True)) 
            
        
        controls_container_jeu.append(ft.Row(controls_cases)) #on ajoute la ligne de cases à la liste
        controls_container_jeu.append(container_separateur) #on ajoute le séparateur entre les lignes de cases
    
    
    container_jeu = ft.Container(  #on créé le container qui va contenir toutes les lignes de cases    
        content=ft.Column(controls_container_jeu), #on met la liste créé précédamment dans une colonne
        bgcolor= color2, #on met le fond du container en bleu clair
        height=685,
        width= 370,
        border_radius = 30,
        padding = ft.padding.only(0,5,0,0),
        margin = ft.margin.only(30))

    
    
##### création de la column_info (colonne droite de la page qui contient l'image d'ampoule et les 2 textes bien/mal placés)


    img_ampoule = ft.Image( #on va chercer l'image d'ampoule dans les fichiers
        src=f"assets/images/ampoule.png", #emplacement de l'image
        width=300,
        height=300)


    
    column_infos = ft.Column([ #on créé une colonne qui va contenir des container avec l'image, textes...
        ft.Container(#on créé un container qui contient l'image afin d'afficher cette dernière
            content = img_ampoule,
            on_click = affichage2_solution), #fonction de test : on affiche la solution en ciquant sur l'image
            
        ft.Container( #container avec comme contenu le nombre de couleurs bien places
            content = ft.Text(
                "0 couleur bien placée", #nombre de couleurs bien placés définis a 0 au départ mais qui peut changer avec differentes fonctions
                size = 30,
                color = text_color_1, 
                font_family = "Kanit"),
            width= 200,
            height=100,
            bgcolor= color1, 
            padding = ft.padding.only(10),
            border_radius = 10,
            alignment=ft.alignment.top_center),
            
        ft.Container(#container avec comme contenu le nombre de couleurs mal places
            content = ft.Text(
                "0 couleur mal placée", #nombre de couleurs mal placés définis a 0 au départ mais qui peut changer avec differentes fonctions
                size = 30,
                color = text_color_1,
                font_family = "Kanit"),
            width= 200,
            height=100,
            bgcolor= color1, 
            padding = ft.padding.only(10),
            border_radius = 10,
            margin = 20,
            alignment=ft.alignment.top_center),
        ],
        
        horizontal_alignment = ft.CrossAxisAlignment.CENTER) #on aligne horizontalement les éléments de la colonne au centre de celle ci
        

        
##### on ajoute les 2 éléments créés précédament dans le page    


    page.add(ft.Row( #on créé une ligne pour que les 2 éléments créés précédament soient sur la meme ligne horizontalement
        [
            container_jeu, 
            column_infos
        ]
    ))
    
    
    
##### création du container_boutons ("ligne" en bas de la page avec les boutons valider, recommencer, et quitter)
    
    
    container_boutons = ft.Container(ft.Row([ #on créé un container qui contient la ligne avec les 4 boutons
        ft.ElevatedButton("X", icon="refresh",height = 50, on_click = callback_reset, bgcolor = "#212524", color = color2),#bouton "recommencer" 
        ft.ElevatedButton("VALIDER", icon="done",height = 50, on_click = callback_valider, bgcolor = "#212524", color = color2),# boutons "valider"
        ft.ElevatedButton("TABLEAU DES SCORES", icon="Filter_List", height = 50, on_click= callback_tableau_score, bgcolor = "#212524", color = color2), #bouton "tableau score"
        ft.ElevatedButton("QUITTER", icon="close", height = 50, on_click= callback_close, bgcolor = "#212524", color = color2), #bouton "quitter"
    ]),
                                     height = 70,
                                    width = 660,
                                    bgcolor = color1,
                                    margin = ft.margin.only(50, 20,0,-20),
                                    padding = ft.padding.only(40),
                                    border_radius = 10,
                                    )
    
    page.add(container_boutons) #on ajoute l'element à la page

    
   
    activer_ligne(0) #on active le clic et le contour des cases de la première ligne pour le premier tour
    
#################################### gestion du tableau des scores ######################################
    
    
    def tri_selection(t) :
        '''fonction pour le tri du tableau de score'''
        for i in range(len(t) -1) :
            i_max = i
            for j in range(i+1, len(t)) :
                if int(t[j][2]) > int(t[i_max][2]) : #utilise le fonctionnement du tri_selection
                    i_max = j
            t[i], t[i_max] = t[i_max], t[i]

        return t


    def add_score_csv(filename, table):
        '''ajoute une nouvelle valeur au tableau des scores et met à jour le fichier csv'''
        
        nom =  (j.nom.replace(" ", "_") + "__________")[:10] #on modifie les informations à enter dans le tableau en sorte qu'elles soient bien elignés (meme nombre de caractères)
        
        score =   ("000" + str(j.score))[-3:]
        couleurs = ("0" + str(j.nb_couleurs))[-2:]
        date = str(datetime.now())[5:-10]
        parties = ("00" + str(j.nb_parties))[-2:] 

        table.append(["000", nom, score, parties, couleurs, date]) #on ajoute le score de la partie actuelle au tableau des scores
        table = [table[0]] + tri_selection(table[1:]) #on tri la liste
        
        for i in range(1, len(table)) :
            classement =  ("000" + str(i))[-3:] 
            table[i][0] = classement #on met à jour le placement des joueurs dans le tableau

        with open(filename, 'w') as f: 
            f.write("") #on vide le fichier csv qui est le tableau des scores

        with open(filename, 'a', newline='') as csvfile:
            for element in table : #on remet tous les éléments auparavant triés dans le fichier
                csvwriter = csv.writer(csvfile)
                csvwriter.writerow(element)        


    
    lv_score = ft.ListView( expand=1, spacing=10, padding=20, width = 500, height = 200) #on créé une liste une "liste view" pour affichier le tableua des scores

    for i in range(0, len(j.table_score)):
        row_score = ft.Row(alignment = ft.MainAxisAlignment.SPACE_AROUND)
        for score in j.table_score[i] :
            row_score.controls.append(ft.Text(score)) #on créé une ligne par ligne dans le tableau des scores
        
        lv_score.controls.append(row_score) #on ajoute cette ligne à la "list view"

############################################### fermeture alertes ################################################


    def close_alert_incomplet(e): #ferme l'alerte indiquant que toutes les couleurs sont pas remplis
        '''ferme alerte_incomplet'''
        alert_incomplet.open = False
        
        page.update()


        
    def close_alert_victoire(e):  #ferme l'alerte indiquant que le joueur a gagné
        '''ferme alert_victoire'''
        alert_victoire.open = False
        reset_jeu(True) #on execute la fontion pour reinitialiser le jeu avec True comme parametre pour ajouter des points au joueur
        
        page.update()
        
    
    
    def close_alert_defaite(e): #ferme l'alerte indiquant que le joueur a perdu
        '''ferme alert_defaite'''
        alert_defaite.open = False 
        reset_jeu(False) #on execute la fontion pour reinitialiser le jeu avec False comme parametre pour ne pas ajouter de points au joueur
        
        page.update()
        

        
    def close_alert_victoire_ordi(e) : #ferme l'alerte indiquant que l'ordinateur a gagné
        '''ferme alert_victoire_ordi'''
        alert_victoire_ordi.open = False 
        reset_jeu(False)  #on execute la fontion pour reinitialiser le jeu avec False comme parametre pour ne pas ajouter de points au joueur

        page.update()
    
    
    def close_alert_tableau_score(e) :
        '''ferme alert_tableau_score'''
        alert_tableau_score.open = False
        
        page.update()
    
    
################################################ alertes ####################################################
    
    alert_incomplet = ft.AlertDialog(#alerte en cas de validation lorsque la propositon est incomplete
            modal = True,
            title=ft.Text("TOUTES LES COULEURS \n NE SONT PAS REMPLIS"), #contenu de l'alerte
            actions=[
                ft.TextButton("OK", on_click= close_alert_incomplet), #execution de la fonction de fermeture de l'alerte au click sur le bouton "ok" de l'alerte
            ],
            actions_alignment=ft.MainAxisAlignment.END,
            open =False, #initialement non affichée
            )
    
    page.add(alert_incomplet) #on ajoute l'alerte de proposition incomplete à la page

    
#####
     
    alert_victoire = ft.AlertDialog( #alerte lors de la victoire du joueur
        modal = True,
        title=ft.Text("Gagné !", size = 40), #conteu de l'alerte
        content=ft.Text(""),
        actions=[
            ft.TextButton("Nouvelle partie", on_click= close_alert_victoire), #execution de la fonction de fermeture de l'alerte au click 
        ],
        actions_alignment=ft.MainAxisAlignment.END,
        open =False, #initialement non affichée
            )
    
    page.add(alert_victoire) #on ajoute l'alerte de victoire du joueur
    
    
#####
    
    alert_defaite = ft.AlertDialog(  #alerte lors de defaite du joueur
        modal = True,
        title=ft.Text("Perdu !", size = 40),
        content=ft.Text(""),
        actions=[
            ft.TextButton("Nouvelle partie", on_click= close_alert_defaite), #execution de la fonction de fermeture de l'alerte au click 
        ],
        actions_alignment=ft.MainAxisAlignment.END,
                open =False, #initialement non affichée
            )
 
    page.add(alert_defaite) #on ajoute l'alerte de defaite du joueur à la page
    
    
#####    
    
    alert_victoire_ordi = ft.AlertDialog( #alerte de victoire de l'ordinateur
        modal = True,
        title=ft.Text("L'ordinateur à gagné !", size = 40),
        content=ft.Text(""),
        actions=[
            ft.TextButton("Nouvelle partie", on_click= close_alert_victoire_ordi),  #execution de la fonction de fermeture de l'alerte au click 
        ],
        actions_alignment=ft.MainAxisAlignment.END,
        open =False, #initialement non affichée
            )
    
    page.add(alert_victoire_ordi)
    

##### 

    alert_tableau_score = ft.AlertDialog( #alerte qui contient le tableau des scores
        modal = False,
        title=ft.Text("Tableau des scores", size = 40),
        content= lv_score, #on met la "list_view" qui contient le tableau des scores en contenu de l'alerte
        actions=[
            ft.TextButton("Fermer", on_click= close_alert_tableau_score),  #execution de la fonction de fermeture de l'alerte au click 
        ],
        actions_alignment=ft.MainAxisAlignment.END,
        open =False, #initialement non affichée
            )
        
    page.add(alert_tableau_score)

    
    
    page.update()
################################################## chrono ################################################
    
    while j.page_ouverte : 
        if j.chrono_en_cours : #tant que la page est ouverte et que le chrono doit etre en cours
            
            
            if (j.tps_jeu)%60 < 10 : #mise à jour du chronometre
                row_infos.controls[1].content.value = "  temps : "+ str(j.tps_jeu//60)+ "'0" + str(j.tps_jeu%60) + "''"
            else :
                row_infos.controls[1].content.value = "  temps : "+ str(j.tps_jeu//60)+ "'" + str(j.tps_jeu%60) + "''"
            j.tps_jeu += 1 #ajout d'une seconde au chronometre
            
        page.update()
        sleep(1) #on attends 1 seconde entre chaque mise à jour du chronometre
        
     
            
######################################        
    
    page.update()


